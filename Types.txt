<gateway compatibility="1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="http://schema.itrsgroup.com/GA4.1.0-170114/gateway.xsd">
    <types>
        <type name="App Dev">
            <sampler ref="Ports Development"></sampler>
            <sampler ref="Process Docker"></sampler>
            <sampler ref="Webmon Logstash"></sampler>
            <sampler ref="Process Logstash"></sampler>
            <sampler ref="Process Mysql"></sampler>
            <sampler ref="Process ActiveMQ"></sampler>
            <sampler ref="Process Packetbeat"></sampler>
            <sampler ref="Process Metricbeat"></sampler>
            <sampler ref="Process Filebeat"></sampler>
        </type>
        <type name="Docker Reg">
            <sampler ref="Ports Docker Reg"></sampler>
            <sampler ref="Webmon Docker"></sampler>
            <sampler ref="Process Logstash"></sampler>
            <sampler ref="Toolkit Docker"></sampler>
        </type>
        <type name="Deployment">
            <sampler ref="Ports Development"></sampler>
            <sampler ref="Process Logstash"></sampler>
            <sampler ref="Process uDeploy"></sampler>
            <sampler ref="Webmon Bitbucket"></sampler>
        </type>
        <type name="UAT">
            <sampler ref="Ports UAT"></sampler>
            <sampler ref="Process uDeploy"></sampler>
            <sampler ref="Process Logstash"></sampler>
            <sampler ref="Process Mysql"></sampler>
            <sampler ref="Process ActiveMQ"></sampler>
            <sampler ref="Process Packetbeat"></sampler>
            <sampler ref="Process Metricbeat"></sampler>
            <sampler ref="Process Filebeat"></sampler>
        </type>
        <type name="ELK">
            <sampler ref="Ports ELK"></sampler>
            <sampler ref="Process ElasticSearch"></sampler>
            <sampler ref="Process Logstash"></sampler>
            <sampler ref="Process Kibana"></sampler>
            <sampler ref="Webmon Logstash"></sampler>
            <sampler ref="Toolkit Disk"></sampler>
        </type>
        <type name="Prod">
            <sampler ref="Ports Prod"></sampler>
            <sampler ref="Process Logstash"></sampler>
            <sampler ref="Process uDeploy"></sampler>
            <sampler ref="Process Mysql"></sampler>
            <sampler ref="Process ActiveMQ"></sampler>
            <sampler ref="Process Packetbeat"></sampler>
            <sampler ref="Process Metricbeat"></sampler>
            <sampler ref="Process Filebeat"></sampler>
        </type>
        <type name="Financial Feed">
            <sampler ref="Webmon Financial Feed "></sampler>
        </type>
    </types>
</gateway>