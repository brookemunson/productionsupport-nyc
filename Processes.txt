<gateway compatibility="1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="http://schema.itrsgroup.com/GA4.1.0-170114/gateway.xsd">
    <samplers>
        <sampler name="Process ElasticSearch">
            <plugin>
                <processes>
                    <processes>
                        <process>
                            <data>
                                <alias>
                                    <data>elasticsearch</data>
                                </alias>
                            </data>
                        </process>
                    </processes>
                </processes>
            </plugin>
        </sampler>
        <sampler name="Process Kibana">
            <plugin>
                <processes>
                    <processes>
                        <process>
                            <data>
                                <alias>
                                    <data>kibana</data>
                                </alias>
                            </data>
                        </process>
                    </processes>
                </processes>
            </plugin>
        </sampler>
        <sampler name="Process uDeploy">
            <plugin>
                <processes>
                    <processes>
                        <process>
                            <data>
                                <alias>
                                    <data>ibm-ucd</data>
                                </alias>
                            </data>
                        </process>
                    </processes>
                </processes>
            </plugin>
        </sampler>
        <sampler name="Process Logstash">
            <plugin>
                <processes>
                    <processes>
                        <process>
                            <data>
                                <alias>
                                    <data>logstash</data>
                                </alias>
                            </data>
                        </process>
                    </processes>
                </processes>
            </plugin>
        </sampler>
        <sampler name="Process Docker">
            <plugin>
                <processes>
                    <processes>
                        <process>
                            <data>
                                <alias>
                                    <data>docker</data>
                                </alias>
                            </data>
                        </process>
                    </processes>
                </processes>
            </plugin>
        </sampler>
        <sampler name="Process Mysql">
            <plugin>
                <processes>
                    <processes>
                        <process>
                            <data>
                                <alias>
                                    <data>mysql</data>
                                </alias>
                            </data>
                        </process>
                    </processes>
                </processes>
            </plugin>
        </sampler>
        <sampler name="Process ActiveMQ">
            <plugin>
                <processes>
                    <processes>
                        <process>
                            <data>
                                <alias>
                                    <data>ActiveMQ</data>
                                </alias>
                            </data>
                        </process>
                    </processes>
                </processes>
            </plugin>
        </sampler>
        <sampler name="Process Packetbeat">
            <plugin>
                <processes>
                    <processes>
                        <process>
                            <data>
                                <alias>
                                    <data>packetbeat</data>
                                </alias>
                            </data>
                        </process>
                    </processes>
                </processes>
            </plugin>
        </sampler>
        <sampler name="Process Metricbeat">
            <plugin>
                <processes>
                    <processes>
                        <process>
                            <data>
                                <alias>
                                    <data>metricbeat</data>
                                </alias>
                            </data>
                        </process>
                    </processes>
                </processes>
            </plugin>
        </sampler>
        <sampler name="Process Filebeat">
            <plugin>
                <processes>
                    <processes>
                        <process>
                            <data>
                                <alias>
                                    <data>filebeat</data>
                                </alias>
                            </data>
                        </process>
                    </processes>
                </processes>
            </plugin>
        </sampler>
    </samplers>
</gateway>