<gateway compatibility="1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="http://schema.itrsgroup.com/GA4.1.0-170114/gateway.xsd">
    <samplers>
        <sampler name="Toolkit Docker">
            <plugin>
                <toolkit>
                    <samplerScript>
                        <data>/usr/bin/docker ps | /bin/grep registry</data>
                    </samplerScript>
                </toolkit>
            </plugin>
        </sampler>
        <sampler name="Toolkit Disk">
            <plugin>
                <disk>
                    <partitions>
                        <partition>
                            <path>
                                <data>/var/lib/elasticsearch</data>
                            </path>
                            <alias>
                                <data>Elastic Search</data>
                            </alias>
                        </partition>
                    </partitions>
                </disk>
            </plugin>
        </sampler>
    </samplers>
</gateway>